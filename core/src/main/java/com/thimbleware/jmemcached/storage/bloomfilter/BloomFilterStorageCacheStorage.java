package com.thimbleware.jmemcached.storage.bloomfilter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

import com.thimbleware.jmemcached.Key;
import com.thimbleware.jmemcached.LocalCacheElement;
import com.thimbleware.jmemcached.storage.CacheStorage;
import com.thimbleware.jmemcached.BloomFilter;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.DataInputStream;

public class BloomFilterStorageCacheStorage implements CacheStorage<Key, LocalCacheElement>{
	
        private BloomFilter<Key> _db;
        private int bfNum;
        static final Charset charset = Charset.forName("UTF-8");
        
        public BloomFilterStorageCacheStorage(int bits,int expElements,int hashNum) {
                _db = new BloomFilter<Key>(bits,expElements,hashNum);
        }

	public BloomFilterStorageCacheStorage(int bits,int expElements,int hashNum,String file_Name) {

                _db = new BloomFilter<Key>(bits,expElements,hashNum); 
                try{
                    InputStream in = new FileInputStream(file_Name);
                    DataInputStream stream = new DataInputStream(in);
                    _db.readFields(stream);
                }catch(Exception e){}
              
	}

	@Override
	public LocalCacheElement putIfAbsent(Key arg0, LocalCacheElement arg1) {
		return null;
	}

	@Override
	public boolean remove(Object arg0, Object arg1) {
		return false;
	}

	@Override
	public LocalCacheElement replace(Key arg0, LocalCacheElement arg1) {
                if(_db.contains(arg0)){
                   put(arg0,arg1); 
                }
		return null;
	}

	@Override
	public boolean replace(Key arg0, LocalCacheElement arg1,
			LocalCacheElement arg2) {
		return false;
	}

	@Override
	public void clear() {
                _db.clear();	        	
	}

	@Override
	public boolean containsKey(Object key) {
                Key k = (Key) key;
                return _db.contains(k);
	}

	@Override
	public boolean containsValue(Object value) {
		return false;
	}

	@Override
	public Set<java.util.Map.Entry<Key, LocalCacheElement>> entrySet() {
		return null;
	}

	@Override
	public LocalCacheElement  get(Object key) {
                Key k = (Key) key;
                String cat = "item";
                if(containsKey(key)){
                    ChannelBuffer outbuf = ChannelBuffers.buffer(4);
                    outbuf.writeBytes(cat.getBytes());
                    LocalCacheElement e = new LocalCacheElement(k);
                    e.setData(outbuf);
                    return e;
                }
                else
		    return null;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public Set<Key> keySet() {
		return null;
	}

	@Override
	public LocalCacheElement put(Key key, LocalCacheElement value) {
                _db.add(key);
		return value;
	}

	@Override
	public void putAll(Map<? extends Key, ? extends LocalCacheElement> m) {
		
	}

	@Override
	public LocalCacheElement remove(Object key) {
		return null;
	}

	@Override
	public int size() {
		return _db.count();
	}

	@Override
	public Collection<LocalCacheElement> values() {
		return null;
	}

	@Override
	public long getMemoryCapacity() {
		return 0;
	}

	@Override
	public long getMemoryUsed() {
		return 0;
	}

	@Override
	public int capacity() {
		return 0;
	}

	@Override
	public void close() throws IOException {

	}

}
